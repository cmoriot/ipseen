# IPseen

To characterize a pool of IPs!

Two version available : 

- version 1 using threshold and manual RDAP analysis (advantage fast) and can be used to run IPs one by one. 
- version 2 using ML (random forrest) and NLP (spacy) to analyse. Longer but better results. Can be used to run one IP or a csv file containing a list of IP addresses.


To access each version, checkout branches :
    - git checkout version1
    - git checkout version2

## Getting started
IPSeen uses external packages :

For Version 1 :
    pip3 install tldextract ipwhois pandas textdistance unidecode deep_translator

For Version 2 :
    pip3 install spacy ipwhois textdistance unidecode pandas tldextract cachetools scikit-learn

## Usage

For Version 1 :
    The ipseen script can be launch with an IP address as parameter.

    Example : ./ipseen.py 42.42.42.42

For Version 2 :
    The script can be launch with an IP address or a IP file list like the example_ip.csv file. 

    Example :

    - ./ipseen.py --ip 42.42.42.42
    - ./ipseen.py --file example_ip.csv